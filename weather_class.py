import requests
import json


class Weather:
    def __init__(self, api_key):
        self.api_key = api_key
        self.history_dict = {}
        self.weather_dict = {}

    def open_requests(self, querystring, take_date, time_file):
        url = "https://weatherapi-com.p.rapidapi.com/" + time_file

        headers = {
            'x-rapidapi-host': "weatherapi-com.p.rapidapi.com",
            'x-rapidapi-key': self.api_key
            }
        response = requests.request("GET", url,
                                    headers=headers, params=querystring)
        sl = {}
        take_date = str(take_date)
        self.weather_dict = response.json()
        for k in self.weather_dict["location"]:
            sl[k] = self.weather_dict["location"][k]
        for k in self.weather_dict["forecast"]["forecastday"][0]["day"]:
            sl[k] = self.weather_dict["forecast"]["forecastday"][0]["day"][k]
        for k in self.weather_dict["forecast"]["forecastday"][0]["astro"]:
            sl[k] = self.weather_dict["forecast"]["forecastday"][0]["astro"][k]
        self.history_dict[take_date] = sl
        print("Data: ", take_date)
        for w in self.history_dict[take_date]:
            print("{}: {}".format(w, self.history_dict[take_date][w]))

    def read_file(self, take_date):
        with open('weather_history.json', 'r') as f:
            data = json.load(f)
            self.history_dict = data
            take_date = str(take_date)
            if take_date in data:
                print("Data: ", take_date)
                for w in data[take_date]:
                    print("{}: {}".format(w, data[take_date][w]))
            else:
                return False

    def write_request(self):
        with open('weather_history.json', 'w') as f:
            json.dump(self.history_dict, f)
