import sys
import os
from datetime import date, timedelta
from weather_class import Weather

if len(sys.argv) < 2 or len(sys.argv) > 3 :
    print("Podano zla ilosc argumentow")
else:

    api_key = sys.argv[1]

    d = date.today()
    t = timedelta(days=1)
    dt = d + t

    filepath = Weather(api_key)

    day = date.fromisoformat(sys.argv[2]) if sys.argv[2:] else dt

    # if sys.argv[2:]:
    #     day = date.fromisoformat(sys.argv[2])
    # else:
    #     day = dt

    if day > d:
        time_file = "forecast.json"
        querystring = {"q": "Wagadugu", "days": "1", "dt": day, "lang": "pl"}
    else:
        time_file = "history.json"
        querystring = {"q": "Wagadugu", "dt": day, "lang": "pl"}

    if os.path.isfile('weather_history.json'):
        if filepath.read_file(day) is False:
            filepath.open_requests(querystring, day, time_file)
            filepath.write_request()
    else:
        filepath.open_requests(querystring, day, time_file)
        filepath.write_request()
